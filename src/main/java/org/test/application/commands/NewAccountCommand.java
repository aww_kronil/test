package org.test.application.commands;

import org.test.application.entity.Account;
import org.test.application.entity.Command;
import org.test.application.storage.AccountStorage;

public class NewAccountCommand extends Command {

    public NewAccountCommand(){
        super("NEWACCOUNT");
    }

    @Override
    public void execute(String... args) {
        if (validate(args)) {
            AccountStorage.getAccountStorage().addAccount(new Account(args[1]));
            System.out.println("OK");
        } else {
            System.out.println("ERROR: Account must be 5 digit positive number");
        }
    }
}
