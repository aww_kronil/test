package org.test.application.entity;

/**
 * Created by Daniil Khromov.
 */
public abstract class Command {

    private final String name;

    public Command(String name) {
        this.name = name;
    }

    public final String getName() {
        return name;
    }

    protected final boolean validate(String... args) {
        if (args.length == 3) {
            if (args[1].matches("\\d{5}")) {
                try {
                    return Double.valueOf(args[2]) > 0;
                } catch (NumberFormatException e) {
                    return false;
                }
            }
            return false;
        } else if (args.length == 2) {
            return (args[1].matches("\\d{5}"));
        } else {
            return false;
        }
    }

    public abstract void execute(String... args);
}
