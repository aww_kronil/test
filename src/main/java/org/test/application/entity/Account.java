package org.test.application.entity;

import java.math.BigDecimal;

/**
 * Created by Daniil Khromov.
 */
public class Account {

    private String name;

    private BigDecimal balance;

    public Account(String name) {
        this.name = name;
        balance = new BigDecimal(0);
    }

    public String getName() {
        return name;
    }

    public void getBalance() {
        System.out.println(balance);
    }

    public void deposit(BigDecimal amount) {
        balance = balance.add(amount);
    }

    public void withdraw(BigDecimal amount) {
        if (balance.compareTo(amount) > 0) {
            balance = balance.subtract(amount);
            System.out.println("OK");
        } else {
            System.out.println("ERROR: Not enough funds");
        }
    }
}
