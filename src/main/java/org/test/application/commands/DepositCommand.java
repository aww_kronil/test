package org.test.application.commands;

import org.test.application.entity.Command;
import org.test.application.storage.AccountStorage;

import java.math.BigDecimal;

public class DepositCommand extends Command {

    public DepositCommand() {
        super("DEPOSIT");
    }

    @Override
    public void execute(String... args) {
        if (validate(args)) {
            if (AccountStorage.getAccountStorage().getAccount(args[1]) != null) {
                AccountStorage.getAccountStorage().getAccount(args[1]).deposit(new BigDecimal(args[2]));
                System.out.println("OK");
            } else {
                System.out.println("ERROR: Account not found");
            }
        } else {
            System.out.println("ERROR: Invalid argument");
        }
    }
}
