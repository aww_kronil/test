package org.test.application.handler;

import org.test.application.entity.Command;
import org.test.application.registry.CommandRegistry;

public class CommandHandler {

    private final CommandRegistry commandRegistry;

    public CommandHandler() {
        commandRegistry = new CommandRegistry();
    }

    public final void registerCommand(Command command) {
        commandRegistry.register(command);
    }

    public void execute(String... args) {
        if (!commandRegistry.execute(args)) {
            System.out.println("ERROR: Command not found");
        }
    }
}
