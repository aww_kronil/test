package org.test.application.registry;

import org.test.application.entity.Command;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Daniil Khromov.
 */
public class CommandRegistry {

    private final Map<String, Command> commandMap = new HashMap<>();

    public final void register(Command command) {
        if (!commandMap.containsKey(command.getName())) {
            commandMap.put(command.getName(), command);
        } else {
            System.out.println("Failed to register command. " +
                    "Command with name "  + command.getName() + " already exists");
        }
    }

    public final boolean execute(String... args) {
        if (args.length > 0 && commandMap.containsKey(args[0].toUpperCase())) {
            commandMap.get(args[0].toUpperCase()).execute(args);
            return true;
        } else {
            return false;
        }
    }
}
