package org.test.application.commands;

import org.test.application.entity.Command;
import org.test.application.storage.AccountStorage;

import java.math.BigDecimal;

public class WithdrawCommand extends Command {

    public WithdrawCommand() {
        super("WITHDRAW");
    }

    @Override
    public void execute(String... args) {
        if (validate(args)) {
            if (AccountStorage.getAccountStorage().getAccount(args[1]) != null) {
                AccountStorage.getAccountStorage().getAccount(args[1]).withdraw(new BigDecimal(args[2]));
            } else {
                System.out.println("ERROR: Account not found");
            }
        } else {
            System.out.println("ERROR: Invalid argument");
        }
    }
}
