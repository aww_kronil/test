package org.test.application.commands;

import org.test.application.entity.Command;
import org.test.application.storage.AccountStorage;

public class BalanceCommand extends Command {

    public BalanceCommand() {
        super("BALANCE");
    }

    @Override
    public void execute(String... args) {
        if (validate(args)) {
            if (AccountStorage.getAccountStorage().getAccount(args[1]) != null) {
                AccountStorage.getAccountStorage().getAccount(args[1]).getBalance();
            } else {
                System.out.println("ERROR: Account not found");
            }
        } else {
            System.out.println("ERROR: Invalid account name");
        }
    }
}
