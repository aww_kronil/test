package org.test.application;

import org.test.application.commands.BalanceCommand;
import org.test.application.commands.DepositCommand;
import org.test.application.commands.NewAccountCommand;
import org.test.application.commands.WithdrawCommand;
import org.test.application.handler.CommandHandler;

import java.util.Scanner;

/**
 * Created by Daniil Khromov.
 */
public class Application {

    public static void main(String[] args) {
        CommandHandler commandHandler = new CommandHandler();
        commandHandler.registerCommand(new NewAccountCommand());
        commandHandler.registerCommand(new DepositCommand());
        commandHandler.registerCommand(new BalanceCommand());
        commandHandler.registerCommand(new WithdrawCommand());

        Scanner in = new Scanner(System.in);

        System.out.println("Input command\n" +
                "Type EXIT to terminate application");

        while (true) {
            String input = in.nextLine();

            if (input.toUpperCase().equals("EXIT")) {
                System.out.println("Terminating application");
                break;
            }
            String[] command = input.trim().split("\\s+");
            commandHandler.execute(command);
        }
    }
}
