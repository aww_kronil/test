package org.test.application.storage;

import org.test.application.entity.Account;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Daniil Khromov.
 */
public class AccountStorage {

    private Map<String, Account> accountMap;

    private static AccountStorage accountStorage;

    private AccountStorage() {
        accountMap = new HashMap<>();
    }

    public static AccountStorage getAccountStorage() {
        if (accountStorage == null) {
            accountStorage = new AccountStorage();
        }
        return accountStorage;
    }

    public void addAccount(Account account) {
        if (!accountMap.containsKey(account.getName())) {
            accountMap.put(account.getName(), account);
        } else {
            System.out.println("ERROR: Account already exists");
        }
    }

    public Account getAccount(String name) {
        return accountMap.get(name);
    }
}
